# 开源GSM定位器实战项目

# 介绍
基于AIR800设计的一款定位器。可以实现GPS定位和基站定位，并传输坐标信息到阿里云IOT平台。

图文完整版文档地址：https://www.yuque.com/lingyao/jing/dvzkai
<a name="UGvrz"></a>

---
![微信图片.jpg](https://images.gitee.com/uploads/images/2019/1008/213037_61f35bb1_2105452.jpeg)
---

<a name="nr7rx"></a>
## 1，开源硬件+软件地址

**嵌入式软件地址：**[https://gitee.com/LingYao01/GSM-Location ](https://gitee.com/LingYao01/GSM-Location )<br />**主控板：**[https://lceda.cn/7a745fc8/gprs-ding-wei-qi](https://lceda.cn/7a745fc8/gprs-ding-wei-qi)<br />**USB转串口（TTL）：**[https://lceda.cn/jixin001/usb-zhuaittl-usb-jie-kou-shu-chu](https://lceda.cn/jixin001/usb-zhuaittl-usb-jie-kou-shu-chu)<br />**Lua-Tools下载地址：**[http://www.openluat.com/Product/gprs/Air202.html](http://www.openluat.com/Product/gprs/Air202.html)<br />**备注：**上述硬件95%的物料可以从立创商城一站式购买（WWW.SZLCSC.COM），80%的物料可以使用嘉立创SMT一站式服务（WWW.SZ-JLC.COM）

<a name="5JAi8"></a>
## 2，硬件相关学习资料

**主控硬件设计资料：**[https://www.bilibili.com/video/av45341487](https://www.bilibili.com/video/av45341487)<br />**原理图+PCB绘制：**[https://docs.lceda.cn/cn/FAQ/Editor/index.html](https://docs.lceda.cn/cn/FAQ/Editor/index.html)<br />**PCB+SMT打样：**[https://docs.lceda.cn/cn/PCB/Order-PCB/index.html](https://docs.lceda.cn/cn/PCB/Order-PCB/index.html)

<a name="mTi18"></a>
## 3，软件相关学习资料

**主控软件设计资料：**[http://wiki.openluat.com/](http://wiki.openluat.com/)<br />**主控问题讨论社区（学习过程中有疑问请到社区提问）：**[http://ask.openluat.com/](http://ask.openluat.com/)<br />**主控软件设计视频教程：**[https://www.bilibili.com/video/av50827315](https://www.bilibili.com/video/av50827315)<br />     [https://www.bilibili.com/video/av50453083](https://www.bilibili.com/video/av50453083)<br />     [https://www.bilibili.com/video/av41012302](https://www.bilibili.com/video/av41012302)<br />**阿里云问题讨论社区：**[https://developer.aliyun.com/group/aliiot](https://developer.aliyun.com/group/aliiot)

<a name="vXtzo"></a>
## 4，联系零妖
欢迎关注公众号，偶尔更新<br />![image.png](https://images.gitee.com/uploads/images/2019/1002/004233_46a5f839_2105452.png)
